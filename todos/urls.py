from django.urls import path
from .views import todo_list, todo_detail, todo_create, todo_update, todo_delete, todo_create_item, todo_update_item

urlpatterns = [
    path("", todo_list, name="todo_list_list"),
    path("<int:id>/", todo_detail, name="todo_list_detail"),
    path("create/", todo_create, name="todo_list_create"),
    path("<int:id>/edit/", todo_update, name="todo_list_update"),
    path("<int:id>/delete/", todo_delete, name="todo_list_delete"),
    path("items/create/", todo_create_item, name="todo_item_create"),
    path("items/<int:id>/edit/", todo_update_item, name="todo_item_update"),
]
