from django.shortcuts import render, redirect
from .models import TodoList, TodoItem
from .forms import TodoListForm, TodoItemForm

# Create your views here.
def todo_list(request):
    todos = TodoList.objects.all()
    context = {
        "todo_list": todos,
    }
    return render(request, "todos/list.html", context)

def todo_detail(request, id):
    todo = TodoList.objects.get(id=id)
    context = {
        "todo_list": todo
    }
    return render(request, "todos/detail.html", context)

def todo_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm()

    context = {
        "form" : form
    }

    return render(request, "todos/create.html", context)

def todo_update(request, id):
    todo = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo)
        if form.is_valid():
            todo = form.save()
            return redirect("todo_list_detail", id=todo.id)
    else:
        form = TodoListForm(instance=todo)

    context = {
        "form" : form
    }

    return render(request, "todos/edit.html", context)

def todo_delete(request, id):
    todo = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")

def todo_create_item(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()


    context = {
        "form" : form,

    }

    return render(request, "todos/item_create.html", context)

def todo_update_item(request, id):
    todo = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo)
        if form.is_valid():
            todo = form.save()
            return redirect("todo_list_detail", id=todo.list.id)
    else:
        form = TodoItemForm(instance=todo)

    context = {
        "form" : form
    }

    return render(request, "todos/item_edit.html", context)
